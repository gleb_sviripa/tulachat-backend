package co.tula.chat.backend.data

class RegisterResponse(val user : User, val token : String)