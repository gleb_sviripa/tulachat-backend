package co.tula.chat.backend.data

/*
 * Created by Gleb
 * TulaCo 
 * 11/30/2017
 */
class AuthResponse (val user : User, val token : String)