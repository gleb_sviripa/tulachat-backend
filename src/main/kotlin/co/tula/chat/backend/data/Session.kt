package co.tula.chat.backend.data

import java.util.*

class Session () {

    var token = generateNewToken()
    var timestamp = Calendar.getInstance().timeInMillis

    fun isValid() : Boolean = Calendar.getInstance().timeInMillis - timestamp > 3600000

    fun updateSession() {
        timestamp = Calendar.getInstance().timeInMillis
    }

    fun newSession() {
        token = generateNewToken()
        updateSession()
    }

    private fun generateNewToken() : String = UUID.randomUUID().toString()
}