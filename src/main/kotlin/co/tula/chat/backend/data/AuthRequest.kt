package co.tula.chat.backend.data

import co.tula.chat.backend.JsonRequired

class AuthRequest(@JsonRequired val login : String, @JsonRequired val password : String) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AuthRequest

        if (login != other.login) return false
        if (password != other.password) return false

        return true
    }

    override fun hashCode(): Int {
        var result = login.hashCode()
        result = 31 * result + password.hashCode()
        return result
    }
}