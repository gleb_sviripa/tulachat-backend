package co.tula.chat.backend.data

/*
 * Created by Gleb
 * TulaCo 
 * 11/30/2017
 */
class ChatMessage (val id : Int, val type : Int, val senderId : Int, val recipientId : Int, val message : String, val link : String, val timeUnit : Long)

enum class ChatMessageType(val value : Int) {
    SYSTEM(0),
    USER(1),
}