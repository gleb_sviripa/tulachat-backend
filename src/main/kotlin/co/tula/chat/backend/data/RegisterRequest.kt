package co.tula.chat.backend.data

import co.tula.chat.backend.JsonRequired

class RegisterRequest(@JsonRequired val login: String,
                      @JsonRequired val password : String,
                      @JsonRequired val firstName : String,
                      @JsonRequired val lastName : String,
                      @JsonRequired val position : String)