package co.tula.chat.backend.data

import co.tula.chat.backend.JsonRequired

/*
 * Created by Gleb
 * TulaCo 
 * 11/30/2017
 */
class User (@JsonRequired val id : Int, @JsonRequired val firstName : String, @JsonRequired val lastName : String, val position : String?) {
}