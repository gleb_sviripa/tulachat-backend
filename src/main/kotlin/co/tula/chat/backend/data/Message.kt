package co.tula.chat.backend.data

class Message(val token : String, val senderId : Int, val recipientId : Int, val text : String, val link : String = "")