package co.tula.chat.backend.domain

import co.tula.chat.backend.data.Message
import co.tula.chat.backend.data.ChatMessage
import co.tula.chat.backend.data.ChatMessageType
import co.tula.chat.backend.toJson
import com.google.gson.Gson
import java.util.*
import kotlin.collections.ArrayList

class MessagingHandler {

    val messagesList = ArrayList<ChatMessage>()

    fun createChatMessage(token : String, type : ChatMessageType, senderId : Int, recipientId : Int, message : String = "", link : String = "") : ChatMessage? {

        val chatMessage = ChatMessage(messagesList.size, type.value, senderId, recipientId, message, link, Calendar.getInstance().timeInMillis)
        messagesList.add(chatMessage)
        return chatMessage
    }

    fun createChatMessage(message : Message) : ChatMessage? = createChatMessage(message.token, ChatMessageType.USER, message.senderId, message.recipientId, message.text, message.link)


    fun getMessagesByUserId(userId : Int) : ArrayList<ChatMessage> = ArrayList(messagesList.filter { it.senderId == userId || it.recipientId == userId })

    fun getChatMessages(userIds : Pair<Int, Int>) : ArrayList<ChatMessage> = ArrayList(messagesList.filter {
        (it.senderId == userIds.first && it.recipientId == userIds.second) || (it.senderId == userIds.second && it.recipientId == userIds.first) })
}