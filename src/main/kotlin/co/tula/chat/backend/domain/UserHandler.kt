package co.tula.chat.backend.domain

import co.tula.chat.backend.data.AuthRequest
import co.tula.chat.backend.data.RegisterRequest
import co.tula.chat.backend.data.User
import co.tula.chat.backend.data.UserData
import com.google.gson.Gson

class UserHandler {

    private val userList = ArrayList<UserData>()

    fun createUser(registerRequest: RegisterRequest) : UserData? {
        if(checkLoginExists(registerRequest.login)) return null
        val user = User(userList.size, registerRequest.firstName, registerRequest.lastName, registerRequest.position)
        val authRequest = AuthRequest(registerRequest.login, registerRequest.password)
        userList.add(UserData(user, authRequest))
        return UserData(user, authRequest)
    }

    fun getUserOnAuth(authRequest: AuthRequest) : UserData? = userList.stream().filter({ it.authRequest == authRequest }).findFirst().orElse(null)

    fun getUserById(id : Int): UserData? = userList.stream().filter({it.user.id == id}).findFirst().orElse(null)

    fun getUsersJson() : String = Gson().toJson(userList.map { it.user })

    private fun checkLoginExists(login : String) : Boolean = userList.stream().filter { it.authRequest.login == login }.count() > 0

}