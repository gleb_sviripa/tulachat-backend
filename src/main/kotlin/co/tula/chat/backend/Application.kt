package co.tula.chat.backend

import co.tula.chat.backend.data.*
import co.tula.chat.backend.domain.MessagingHandler
import co.tula.chat.backend.domain.UserHandler
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import io.vertx.core.Vertx
import io.vertx.core.http.ServerWebSocket
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.BodyHandler

fun main(args: Array<String>) {
    val vertx = Vertx.vertx()
    val server = vertx.createHttpServer()
    val port = 80
    val router = Router.router(vertx)
    val messagingHandler = MessagingHandler()
    val userHandler = UserHandler()
    val socketMap = HashMap<Int, ServerWebSocket>()
    router.route().handler(BodyHandler.create())
    router.get("/").handler { it.response().end("<h1>TulaCo Chat</h1>") }
    router.post("/auth").handler { routingContext ->
        Gson().fromJsonWithHandling(routingContext.bodyAsString, AuthRequest::class.java,
                {
                    val user = userHandler.getUserOnAuth(it)
                    if (user != null) routingContext.response().end(AuthResponse(user.user, user.session.token).toJson())
                    else routingContext.response().setStatusCode(500).end("{\"error\":\"Wrong credentials\"}")
                },
                { routingContext.response().setStatusCode(500).end("{\"error\":\"Request error\"}") })
    }
    router.post("/register").handler { routingContext ->
        Gson().fromJsonWithHandling(routingContext.bodyAsString, RegisterRequest::class.java,
                {
                    val userData = userHandler.createUser(it)
                    if (userData != null) routingContext.response().end(AuthResponse(userData.user, userData.session.token).toJson())
                    else routingContext.response().setStatusCode(500).end("{\"error\":\"User already exists\"}")
                },
                { routingContext.response().setStatusCode(500).end("{\"error\" : \"Request error\"}") })
    }
    router.get("/messages").handler { it.response().end(messagingHandler.messagesList.toJson()) }
    router.get("/messages/:userId").handler { it.response().end(messagingHandler.getMessagesByUserId(Integer.valueOf(it.request().getParam("userId"))).toJson())}
    router.get("/users").handler { it.response().end(userHandler.getUsersJson()) }
    router.get("/exit").handler {System.exit(0)}

    server.websocketHandler({ webSocket ->

        webSocket.textMessageHandler({
            val message = it.fromJson(Message::class.java)
            val chatMessage = messagingHandler.createChatMessage(message)
            if(chatMessage != null) {
                socketMap.put(chatMessage.senderId, webSocket)
                webSocket.sendObject(chatMessage)
                socketMap[chatMessage.recipientId]?.sendObject(chatMessage)
                webSocket.closeHandler { socketMap.remove(chatMessage.senderId) }
            } else {
                webSocket.sendObject(Error("Can't create message"))
            }
        })

    }).requestHandler({ router.accept(it) })
    server.listen(port)
}