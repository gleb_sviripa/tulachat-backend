package co.tula.chat.backend

import co.tula.chat.backend.data.AuthRequest
import com.google.gson.*
import com.google.gson.reflect.TypeToken
import io.vertx.core.http.ServerWebSocket
import io.vertx.core.http.WebSocketBase
import java.lang.reflect.Type

fun ServerWebSocket.sendObject (o : Any): WebSocketBase = this.writeTextMessage(Gson().toJson(o))

fun <T> Gson.fromJsonWithHandling(json : String, classType : Class<T>, successHandler : (T) -> Unit, errorHandler: () -> Unit) {
    try {
        val o = this.fromJson(json, classType)
        if(o != null && validateJsonObject(o)) successHandler(o) else errorHandler()
    } catch (exception : JsonSyntaxException) {
        errorHandler()
    }
}

@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
annotation class JsonRequired

fun validateJsonObject(o : Any) : Boolean {
    o::class.java.declaredFields.forEach { f ->
        if (f.getAnnotation(JsonRequired::class.java) != null) {
            f.isAccessible = true
            if(f.get(o) == null) return false
        }
    }
    return true
}

fun Any.toJson() : String = Gson().toJson(this)

fun <T> String.fromJson(classType: Class<T>) : T = Gson().fromJson(this, classType)
